package damasgame;

import java.io.Serializable;

/**
 *
 * @author Ana Belén
 */
public class Jugador implements Serializable {

    private static final long serialVersionUID = 1234567L;
    /**
     * Nombre del jugador
     */
    private String name;

    /**
     * Color de las fichas con las que juega
     */
    private boolean color;
    
    /**
     * Si va ganando
     */
    private boolean ganador;
    
    /**
     * Numero de fichas que le quedan
     */
    private int numPieces;
    
    private boolean turno;

    /**
     * Constructor
     * @param nam
     * @param c; true si es blanco, false si es negro
     * @param gan si el jugador es ganador
     * @param turno
     */
    public Jugador (String nam, boolean c, boolean gan, boolean turno) {
        this.name = nam;
        this.color = c;
        this.ganador = gan;
        this.numPieces = 12;
        this.turno = turno;
    }

    /**
     * Consigue el color de las fichas del jugador
     * @return color
     */
    public boolean getColor () {
        return color;
    }
    
    /**
     * Asigna el color a las fichas del jugador
     * @param val 
     */
    public void setColor (boolean val) {
        this.color = val;
    }

    /**
     * Consigue el estado en el que se encuentra el jugador
     * @return ganador
     */
    public boolean getGanador () {
        return ganador;
    }
    
    /**
     * Asigna el estado del jugador
     * @param val 
     */
    public void setGanador (boolean val) {
        this.ganador = val;
    }
    
    /**
     * Consigue el nombre del jugador
     * @return name
     */
    public String getName () {
        return name;
    }
    
    /**
     * Asigna el nombre del jugador
     * @param val 
     */
    public void setName (String val) {
        this.name = val;
    }
    
    /**
     * Consigue el numero de fichas que le quedan
     * @return numPieces
     */
    public int getNumPieces () {
        return numPieces;
    }
    
    /**
     * Asigna el numero de fichas 
     * @param val 
     */
    public void setNumPieces (int val) {
        this.numPieces = val;
    }

    /**
     *
     * @return
     */
    public boolean isGanador() {
        return ganador;
    }

    /**
     *
     * @param turno
     */
    public void setTurno(boolean turno) {
        this.turno = turno;
    }

    /**
     *
     * @return
     */
    public boolean isTurno() {
        return turno;
    }
    
    @Override
    public String toString() {
        return "Jugador{" + "name=" + name + ", color=" + color + ", ganador=" + ganador + ", numPieces=" + numPieces + '}';
    }
    
    
}

