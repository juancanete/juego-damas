package damasgame;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;

/**
 *
 * @author Juan
 */
public class Partida {

    //private static Tablero tablero;
    private static Jugador jugador1, jugador2;
    private static Reglas reglas;

    private int numJugadores;

    //Punctuations
    private static Integer puntJug1 = 0, puntJug2 = 0;

    private static final String FILENAME = "saveGame.bin";
    private static final String FILEPUNCT = "savePunctuation.bin";

    private static boolean isEnglishVersion;

    private static int winner;

    //true blancas false negras
    private boolean turno;

    /**
     *
     * @param isEnglishVersion
     */
    public Partida(boolean isEnglishVersion) {
        //Partida.tablero = null;
        if (isEnglishVersion) {
            Partida.reglas = new EnglishVersion();
        } else {
            Partida.reglas = new SpanishVersion();
        }

        Partida.jugador1 = null;
        Partida.jugador2 = null;
        this.isEnglishVersion = isEnglishVersion;
        this.numJugadores = 0;
        this.winner = 0;
        turno = true;
    }

    /**
     *
     * @param jug
     * @param isEnglishVersion
     */
    public Partida(Jugador jug, boolean isEnglishVersion) {

        if (isEnglishVersion) {
            Partida.reglas = new EnglishVersion();
        } else {
            Partida.reglas = new SpanishVersion();
        }

        Partida.jugador1 = jug;
        Partida.jugador2 = null;
        this.isEnglishVersion = isEnglishVersion;
        this.numJugadores = 1;
        this.winner = 0;
        turno = true;
    }

    /**
     *
     * @param jug1
     * @param jug2
     * @param isEnglishVersion
     * @param turno
     */
    public Partida(Jugador jug1, Jugador jug2, boolean isEnglishVersion, boolean turno) {

        if (isEnglishVersion) {
            Partida.reglas = new EnglishVersion();
        } else {
            Partida.reglas = new SpanishVersion();
        }

        Partida.jugador1 = jug1;
        Partida.jugador2 = jug2;
        this.isEnglishVersion = isEnglishVersion;
        this.numJugadores = 2;
        this.winner = 0;
        turno = true;
    }

    /**
     * Get instance of TablaPeriodica. This class is a singleto
     *
     * @param jug
     * @return
     */
    public int remainingPiecesPlayer(Jugador jug) {

        boolean color = jug.getColor();
        int pieces = 0;

        Iterator<Peon> it = reglas.getFichas().iterator();
        Peon peon;

        while (it.hasNext()) {
            peon = it.next();

            if (peon.getColor() == color) {
                pieces++;
            }
        }

        return pieces;
    }

    /**
     * Comprueba si ha habido tablas
     *
     */
    public void checkDraw() {
        if (reglas.getMovimientos() == 100) {
            jugador1.setGanador(true);
            jugador1.setGanador(true);
        }
    }

    /**
     * Comprueba si ha habido un ganador. 1 gana Jug1, 2 gana Jug2, 3 hay
     * empate, 0 no hay ganador
     *
     * @return
     */
    public int checkWinner() {

        if (jugador1.getGanador() && jugador2.getGanador()) {
            return 3;
        } else if (jugador1.getGanador()) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     *
     * @return
     */
    public int getWinner() {
        return winner;
    }

    /**
     *
     * @param winner
     */
    public void setWinner(int winner) {
        this.winner = winner;
    }

    /**
     *
     * @param jug1
     * @param jug2
     */
    public void checkStatusGame(Jugador jug1, Jugador jug2) {

        int piecesJug1 = this.remainingPiecesPlayer(jug1);
        int piecesJug2 = this.remainingPiecesPlayer(jug2);

        if ((piecesJug1 == 1) && (piecesJug2 == 1)) {
            System.out.println("Juego empatado");
        }
    }

    /**
     * Guarda el estado del juego en un fichero
     */
    public void saveGame() {

        try {
            
            FileOutputStream fileOut = new FileOutputStream(FILENAME);
            ObjectOutputStream objGame = new ObjectOutputStream(fileOut);
            objGame.writeObject(Partida.reglas);
            objGame.writeObject(Partida.jugador1);
            objGame.writeObject(Partida.jugador2);
            
            if(isEnglishVersion)
            {
                objGame.writeObject("english");
            }else
            {
                objGame.writeObject("spanish");
            }
            objGame.writeObject(Integer.parseInt("0"));
            objGame.flush();
            objGame.close();

        } catch (IOException e) {
            System.out.println("Error al guardar fichero: " + e);
        }

    }

    /**
     * Recupera el estado de un juego un fichero
     */
    public static void recoveryGame() {

        try {
            
            FileInputStream fileInput = new FileInputStream(FILENAME);
            ObjectInputStream objInGame = new ObjectInputStream(fileInput);
            Partida.reglas = null;
            Partida.reglas = (Reglas) objInGame.readObject();
            Partida.jugador1 = (Jugador) objInGame.readObject();
            Partida.jugador2 = (Jugador) objInGame.readObject();
            
            String english = (String) objInGame.readObject();
            
            //Set type game
            if(english.equalsIgnoreCase("english"))
            {
                Partida.isEnglishVersion = true;
            }else
            {
                Partida.isEnglishVersion = false;
            }
            
            Partida.winner = (Integer) objInGame.readObject();

        } catch (IOException e) {
            System.out.println("Error lectura fichero: " + e);
        } catch (ClassNotFoundException ex) {
            System.out.println("Error al obtener el objeto: " + ex);
        }
    }

    /**
     * Guarda la puntuacion de un juego en un archivo
     */
    public void savePunctuationGame() {
        try {
            FileOutputStream fileOut = new FileOutputStream(FILEPUNCT);
            ObjectOutputStream objPGame = new ObjectOutputStream(fileOut);
            objPGame.writeObject(Partida.puntJug1);
            objPGame.writeObject(Partida.puntJug2);            
            objPGame.flush();
            objPGame.close();

        } catch (IOException e) {
            System.out.println("Error al guardar fichero: " + e);
        }
    }

    /**
     *
     * @return
     */
    public static Reglas getReglas() {
        return reglas;
    }

    /**
     *
     * @return
     */
    public static Jugador getJugador1() {
        return jugador1;
    }

    /**
     *
     * @return
     */
    public static Jugador getJugador2() {
        return jugador2;
    }

    /**
     *
     * @return
     */
    public int getNumJugadores() {
        return numJugadores;
    }

    /**
     *
     * @return
     */
    public static int getPuntJug1() {
        return puntJug1;
    }

    /**
     *
     * @param puntJug1
     */
    public static void setPuntJug1(int puntJug1) {
        Partida.puntJug1 = puntJug1;
    }

    /**
     *
     * @return
     */
    public static int getPuntJug2() {
        return puntJug2;
    }

    /**
     *
     * @param puntJug2
     */
    public static void setPuntJug2(int puntJug2) {
        Partida.puntJug2 = puntJug2;
    }

    public boolean getTurno() {
        return turno;
    }

    public void setTurno(boolean turno) {
        this.turno = turno;
    }

    /**
     * Get instance of Reglas. This class is a singleto
     *
     * @return n
     */
    public static Reglas getInstance() {
        if (reglas == null) {
            reglas = new EnglishVersion();
        }

        return reglas;
    }

    public boolean getIsEnglishVersion() {
        return isEnglishVersion;
    }

    @Override
    public String toString() {
        StringBuffer fichas = new StringBuffer();
        String stReturnable = "";

        try {
            Iterator it = reglas.getFichas().iterator();
            while (it.hasNext()) {
                fichas.append(it.next().toString());
                fichas.append("\n");
            }

            stReturnable = new String(fichas);
        } catch (NullPointerException ex) {
            System.out.println("No hay elementos: " + ex);
        }

        return stReturnable;
    }
}
