/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package damasgame;

/**
 *
 * @author masterinftel22
 */
abstract class Reglas extends Tablero {
    
    private int movimientos = 0;
    private boolean pierdeMachine = false;
    int i = 0;
    public Reglas () {
        super();
    }
    
    abstract boolean intentarMovimiento (Position posFicha, Position nCasilla); 
      
    abstract public boolean defaultMove (Position pos);
    
    abstract public void computerTurn();
    
    abstract public int resultado();
    
    
    /**
    * Funcion que dirije la partida
     * @param posFicha
     * @param posMovimiento
    */
    public void play(Position posFicha, Position posMovimiento, Partida partida){
        
        partida.setWinner(resultado());
        
        if( partida.getWinner() != 0){
            System.out.println("Partida finalizada");
            
            if(partida.getWinner() == 3)
                System.out.println("Empate");
            else if(partida.getWinner() == 1)
                System.out.println("Ganador jugador máquina");
            else if(partida.getWinner() == 2)
                System.out.println("Ganador jugador");
        }
        else 
        {
            if(!getPiece(posFicha).getColor())
            {
                if(intentarMovimiento(posFicha, posMovimiento))
                    computerTurn();
            }
            
        }
    }
    
    /**
     * Comprueba si la posicion esta dentro del tablero. true si esta dentro false si no lo esta
     * @param pos
     * @return
     */
    public boolean outBound (Position pos){
        
        if(pos.getX() <= 7 && pos.getX() >= 0  && pos.getY() <= 7 && pos.getY() >= 0)
            return true;
        else
            return false;
    }
    
    /**
     *
     * @param propio
     * @param enemigo
     * @return
     */
    public boolean couldEat(Peon propio, Peon enemigo){
        
        Boolean posible = false;
        
        if(propio.getColor() != enemigo.getColor()){
            int difx = enemigo.getPosition().getX() - propio.getPosition().getX();
            int dify = enemigo.getPosition().getY() - propio.getPosition().getY();

            Position toEatPosition = new Position (enemigo.getPosition().getX() + difx, enemigo.getPosition().getY() + dify);
            
            if(outBound(toEatPosition))
            {
                if(isFree(toEatPosition))
                {
                    posible = true;
                }
            }
        }
        
        return posible;
    }
    
    public int getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(int movimientos) {
        this.movimientos = movimientos;
    }

    public void setPierdeMachine(boolean pierdeMachine) {
        this.pierdeMachine = pierdeMachine;
    }
    
    public boolean isPierdeMachine() {
        return pierdeMachine;
    }
}