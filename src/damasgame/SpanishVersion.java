/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package damasgame;

import java.io.Serializable;
import static java.lang.Math.abs;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author masterinftel21
 */
public class SpanishVersion extends Reglas implements Serializable {
    
    /**
     *
     */
    
    private static final long serialVersionUID = 1234567L;
    private static Position posEaten;
            
    /**
     *
     */
    public SpanishVersion(){
        super ();
        LinkedList <Peon> fichas = new LinkedList<>();
        LinkedList <Peon> computer;
        
        for(int x=0; x <= 6; x = x+2){
            
            fichas.add(new Peon(true, 0, x));
            fichas.add(new Peon(true, 2, x));
            fichas.add(new Peon(false, 6, x));
            
        }
         
        for(int x=1; x <= 7; x = x+2){
            
            fichas.add(new Peon(true, 1, x));
            fichas.add(new Peon(false, 5, x));
            fichas.add(new Peon(false, 7, x));
        }
        
        super.setFichas(fichas);
        computer = crearListaComputer(fichas);
        super.setComputer(computer);
        
    }
    
    public LinkedList<Peon> crearListaComputer(LinkedList<Peon> fichas){
        LinkedList <Peon> computer = new LinkedList<>();
        
        for(Peon p: fichas)
        {
            if(p.getColor() == true)
                computer.add(p);
        }
        
        return computer;
    }
    
   /**
     * Comprueba todos los parámetros para que un movimiento sea posible
     * @param posFicha
     * @param nCasilla
     */
    @Override
        public boolean intentarMovimiento(Position posFicha, Position nCasilla) {
        Peon pieza = getPiece(posFicha);
        Boolean movimiento = false;
        
        if (pieza == null)
            System.out.println("No hay ficha para mover ahi");
        else
        {   
            
            if(!isPeon(pieza)){
                
                int movP = moverPeon(pieza, nCasilla);
                if(movP == 1)
                {    
                    System.out.println("movimiento permitido");
                    moverFicha(pieza, nCasilla);
                    
                    if(peonMeta(pieza))
                        cambiarADama(pieza);
                    
                    setMovimientos(getMovimientos()+1);
                    movimiento = true;
                }
                else if( movP == 2){
                    movimiento = true;
                }
                else
                {
                    System.out.println("movimiento no permitido");
                }
            }
            else if(isPeon(pieza)){
                
                if(moverDama(pieza, nCasilla))
                {
                    System.out.println("movimiento permitido");
                    moverFicha(pieza, nCasilla);
                    
                    if(peonMeta(pieza))
                        cambiarADama(pieza);
                    
                    setMovimientos(getMovimientos()+1);
                    movimiento = true;
                }
                else
                {
                    System.out.println("movimiento no permitido");
                    movimiento = false;
                }
            }
        }
        
        return movimiento;
    }
    
    /**
     *
     * @param propio
     * @param enemigo
     * @return
     */
    public boolean couldEatPeon(Peon propio, Peon enemigo){
        
        Boolean posible = false;
        
        if(propio.getColor() != enemigo.getColor()){
            int difx = enemigo.getPosition().getX() - propio.getPosition().getX();
            int dify = enemigo.getPosition().getY() - propio.getPosition().getY();

            Position toEatPosition = new Position (enemigo.getPosition().getX() + difx, enemigo.getPosition().getY() + dify);
            
            if(outBound(toEatPosition))
            {
                if(isFree(toEatPosition))
                {
                    posible = true;
                }
            }
        }
        
        return posible;
    }
    
    public boolean couldEatDama(Peon propio, Peon enemigo){
        
        Boolean posible = false;
        
        if (propio.getColor() != enemigo.getColor()) {
            int difx = enemigo.getPosition().getX() - propio.getPosition().getX();
            int dify = enemigo.getPosition().getY() - propio.getPosition().getY();

            Position toEatPosition = new Position(enemigo.getPosition().getX() + difx, enemigo.getPosition().getY() + dify);

            if (isFree(toEatPosition)) {
                posible = true;
            }
        }
        
        return posible;
    }
    
    public boolean outBound (Position pos){
        
        if(pos.getX() <= 7 && pos.getX() >= 0  && pos.getY() <= 7 && pos.getY() >= 0)
            return true;
        else
            return false;
    }
    
    /**
     * Devuelve true si el peon llega al extremo contrario del tablero, False para lo contrario
     * @param peon
     * @return
     */
    public boolean peonMeta(Peon peon){
        if(peon.getColor() && peon.getPosition().getX() == 7){
            return true;
        }
        else if (!peon.getColor() && peon.getPosition().getX() == 0){
            return true;
        }
        else
            return false;
    }
    
    /**
     * Devuelve un objeto dama a partir de un peon
     * @param peon
     */
    public void cambiarADama(Peon peon)
    {
        Peon aux = peon;
        eatPiece(peon.getPosition());
        
        añadirDama(aux);
    }
    
    /**
     * Consulta las resticcionoes para mover un peon. Devuelve true si se puede mover, false si no es posible.
     * @param peon
     * @param pos
     * @return
     */
    public int moverPeon(Peon peon, Position pos){
        
        int dify = pos.getY() - peon.getPosition().getY();
	int difx = pos.getX() - peon.getPosition().getX();
        int posible = 0;
        
        if(abs(difx) == 1 && abs(dify) == 1)
        {
            if(isFree(pos)){//posicion libre mueve ahi
                if(!peon.getColor() && difx<0)
                {
                       posible = 1;
                }
                else if(peon.getColor() && difx>0)
                {
                       posible = 1;
                }
            }
            else if(!isFree(pos))
            { ///posicion ocupada la come y salta una posicion
                if(!peon.getColor() && difx<0)
                {
                    if(couldEatPeon(peon, getPiece(pos)))//Se puede comer? hay fichas detras?
                    {
                            eatPiece(pos);
                            moverFicha(peon, new Position(pos.getX()-1, pos.getY()+dify));
                            
                            if(peonMeta(peon))
                                cambiarADama(peon);
                            
                            posible = 2;
                    }
                }
                else if(peon.getColor() && difx>0)
                {
                    if(couldEatPeon(peon, getPiece(pos)))//Se puede comer? hay fichas detras?
                    {
                        eatPiece(pos);
                        moverFicha(peon, new Position(pos.getX()+1, pos.getY()+dify));
                        
                        if(peonMeta(peon))
                                cambiarADama(peon);
                        
                        posible = 2;
                    }
                }

            }
        }
        return posible;
    }
    
    /**
     * Consulta las resticcionoes para mover una dama. Devuelve true si se puede 
     * mover, false si no es posible.
     * @param peon
     * @param pos
     * @return
     */
    public boolean moverDama(Peon peon, Position pos){
        int dify = pos.getY() - peon.getPosition().getY();
	int difx = pos.getX() - peon.getPosition().getX();
        
        Peon enemigo = getPiece(pos);
        
        boolean posible = false;
        
        if(abs(difx) == abs(dify))
        {
            if(isFree(pos))
            {
                posible = true;
            }else if(!isFree(pos) && couldEatDama(peon, getPiece(pos)))
            {
                if (difx < 0)
                {
                    difx = difx/difx;
                    difx = -difx;
                }else
                {
                    difx = difx/difx;
                }
                
                if (dify < 0)
                {
                    dify = dify/dify;
                    dify = -dify;
                }else
                {
                    dify = dify/dify;
                }
                
                Position toEatPosition = new Position (enemigo.getPosition().getX() + difx, enemigo.getPosition().getY() + dify);
                eatPiece(pos);
                moverFicha(peon, new Position(toEatPosition.getX(), toEatPosition.getY()));
                posible = false;
            }
        }
        return posible;
    }
    
    /**
     * Movimiento por defecto (en diagonal hacia delante)
     * @param pos 
     * @return  
     */
    @Override
    public boolean defaultMove (Position pos) {
        
        //Posicion de mi peon
        int x = pos.getX();
        int y = pos.getY();
        Position cont_der;
        Position cont_izq;
        Boolean eatDer = false;
        Boolean eatIzq = false;
        
        boolean movimiento = false;
        
        
        //Mira el color de mi peon
        Peon peon = getPiece(pos);
        
        if(ruleSpanishEater() != null)
        {
            Peon eater = ruleSpanishEater();
            eatPiece(posEaten);
            
            int difY = eater.getPosition().getY() - posEaten.getY();
            
            if(difY < 0)
            {
                eater.setPosition(new Position(posEaten.getX() +1, posEaten.getY() + 1));
                        if(peonMeta(peon))
                            cambiarADama(peon);
            }else
            {
                eater.setPosition(new Position(posEaten.getX() +1, posEaten.getY() - 1));
                if(peonMeta(peon))
                            cambiarADama(peon);
            }
            
            movimiento = true;
            
            System.out.println("ME PUEDO COMER UNA FICHA");
        }else if (peon.getColor()){
            
            cont_der = new Position(x + 1, y + 1);
            cont_izq = new Position(x + 1, y - 1);
            
            if(!isFree(cont_der))
                eatDer = couldEat(peon, getPiece(cont_der));
            
            if(!isFree(cont_izq))
                eatIzq = couldEat(peon, getPiece(cont_izq));

            //En el caso de que no pueda comer ficha muevo sin comer
            if (!eatDer && !eatIzq) 
            {
                //Para las blancas
                if (peon.getColor()) 
                { 
                    //Miro si puedo moverlo a una casilla vacia y dentro de los limites
                    if (isFree(cont_der) && outBound(cont_der))
                    {
                        peon.setPosition(cont_der);
                        movimiento = true;
                        if(peonMeta(peon))
                            cambiarADama(peon);
                    }
                    else if (isFree(cont_izq) && outBound(cont_izq))
                    {
                        peon.setPosition(cont_izq);
                        movimiento = true;
                        if(peonMeta(peon))
                            cambiarADama(peon);
                    }
                    else 
                    {
                        peon.setPosition(pos); //Si no he podido comer ni moverme a una posicion vacia, me quedo en mi posicion
                    }
                }
            }
            
        }

        return movimiento;
    }
    
    private Peon ruleSpanishEater()
    {
        Peon eater = null;
        
        Iterator <Peon> it = super.getComputer().iterator();
        
        while (it.hasNext()) {
            
            Peon currentPiece = it.next();
            
            
            if ((!isFree(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1)))
                    || (!isFree(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1)))) {
                
                if ((getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1)) != null) 
                        && (currentPiece.getColor()!=getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1)).getColor())
                        && !isFree(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1))) {
                    if (currentPiece instanceof Peon) {
                        if (couldEatPeon(currentPiece, getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1)))) {
                            eater = currentPiece;
                            posEaten = new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1);
                            break;
                        }
                    } else {
                        if (couldEatDama(currentPiece, getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1)))) {
                            eater = currentPiece;
                            posEaten = new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() + 1);
                            break;
                        }
                    }
                }

                if ((getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1)) != null) 
                        &&(currentPiece.getColor()!=getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1)).getColor())
                        && !isFree(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1))) {
                    if (currentPiece instanceof Peon) {
                        if (couldEatPeon(currentPiece, getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1)))) {
                            eater = currentPiece;
                            posEaten = new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1);
                            break;
                        }
                    } else {
                        if (couldEatDama(currentPiece, getPiece(new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1)))) {
                            eater = currentPiece;
                            posEaten = new Position(currentPiece.getPosition().getX() + 1, currentPiece.getPosition().getY() - 1);
                            break;
                        }
                    }
                }
            }
        }
        
        return eater;
    }
    
    public int isThereWinner()
    {
      
        return 1;
    }

    @Override
    public void computerTurn() {
        LinkedList <Peon> nComputer = crearListaComputer(super.getFichas());
            super.setComputer(nComputer);
            
            boolean movComputer;
            
            do
            {
                int size = getComputer().size();
                System.out.println(size);
                double elem = Math.random()*size;
                System.out.println(elem);
                Peon piezaComputer = getComputer().get((int)elem);
                System.out.println("Peon antes: " + piezaComputer);
                movComputer = defaultMove(piezaComputer.getPosition());
                System.out.println("Peon despues: " + piezaComputer);
            }while(!movComputer);
    }

    public static Position getPosEaten() {
        return posEaten;
    }

    public static void setPosEaten(Position posEaten) {
        SpanishVersion.posEaten = posEaten;
    }

    @Override
    public int resultado() {
        int sizeComp = getComputer().size();
        int sizeJug = getFichas().size() - sizeComp;
        
        if(sizeComp == sizeJug && sizeJug == 1)
            return 3;
        else if (isPierdeMachine())
            return 2;
        else if (getMovimientos() > 100)
            return 3;
        else if (sizeComp == 0)
            return 2;
        else if (sizeJug == 0)
            return 1;
        else
            return 0;
    }
    
    
}
