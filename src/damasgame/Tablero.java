package damasgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author VictorCG
 */
public class Tablero implements Serializable {

    private static final int CASILLASTAB = 8;
    private static final int FICHAS = 12;
    private LinkedList<Peon> fichas = new LinkedList<>();
    private LinkedList<Peon> computer = new LinkedList<>();
    
    private static final long serialVersionUID = 1234567L;

    /**
     * Constructor
     * @param fichas
     */
    public Tablero () {
            
    }

    /**
     * Obtiene la lista de fichas
     * @return
     */
    public LinkedList<Peon> getFichas() {
        return fichas;
    }
    
    /**
     * Instancia una lista a la lista de fichas
     * @param fichas
     */
    public void setFichas(LinkedList<Peon> fichas) {
        this.fichas = fichas;
    }
    
    /**
     * Obtiene la lista de fichas del ordenador
     * @return
     */
    public LinkedList<Peon> getComputer() {
        return computer;
    }
    
    /**
     * Instancia una lista a la lista de fichas del ordenador
     * @param computer
     */
    public void setComputer(LinkedList<Peon> computer) {
        this.computer = computer;
    }
    
    /**
     * Añadimos la dama a la lista de piezas cuando sea necesario
     * @param peon
     */
    public void añadirDama(Peon peon){
        fichas.add(new Dama(peon.getColor(), peon.getPosition()));
    }
   
    /**
     * IS FREE SOLO CHEQUEA QUE ESTE LIBRE UNA POSICION DADA, PARA HACER QUE SEA UTIL HABRIA QUE HACER UNA FUNCION
     * EN LA QUE SE LE PASE EL COLOR DE LA FICHA QUE PREGUNTA. 
     * PARA QUE DIGA SI PUEDE MOVER POR ENCIMA DE ELLA EN CASO DE SEA FICHA ENEMIGA Y
     * QUE ESTE LIBRE LA SIGUIENTE POSICION O SI ES FICHA PROPIA QUE NO PUEDA MOVER
     * @param pos
     * @return
     */
    public boolean isFree (Position pos) {
        
        boolean free = true;
        
        for (Peon p: fichas){
            if(pos.getX() == p.getPosition().getX() && pos.getY() == p.getPosition().getY()){
                free = false;
            }
        }
        return free;
    }

    /**
     * Delete a piece
     * @param pos
     */
    public void eatPiece (Position pos) {
        
        
        fichas.remove(getPiece(pos));
        
        computer.remove(getPiece(pos));
    }

    /**
     * Nuestra la lista de elementos
     * @param lista
     */
    public void showList(LinkedList<Peon> lista){
        for (Peon p: lista){
                System.out.println(p);
            }
    }    

    /**
     *
     * @param pos
     * @return
     */
    public Peon getPiece(Position pos) {

        Peon peon = null;

        if (!isFree(pos)) {
            for (Peon p : fichas) {
                if (pos.getX() == p.getPosition().getX() && pos.getY() == p.getPosition().getY()) {
                    peon = p;
                }
            }
        }
        
        
        return peon;
    }

    /**
     *
     * @param peon
     * @return
     */
        public Position getPosition (Peon peon) {
        return peon.getPosition();
    }
        
    /**
     * Devuelve si es instancia de dama
     * @param ficha
     * @return
     */
    public boolean isPeon (Object ficha){
        return (ficha instanceof Dama);
    }
    
    
    /**
     * Cambia la posicion de la ficha
     * @param peon
     * @param posicion
     */
    public void moverFicha(Peon peon, Position posicion){

        peon.setPosition(posicion);
    }
        
    @Override
    public String toString() {
        StringBuffer stTablero = new StringBuffer("|");
        String stReturnable;
        
        for (int i = 0; i < CASILLASTAB;i++)
        {
            for (int j = 0; j < CASILLASTAB; j++)
            {
                if(i % 2 == 0)
                {
                    if((j%2 == 0) && (this.isFree(new Position(i, j))))
                    {
                        stTablero.append("[ ]|");
                    }else if ((j%2 == 0) && (!this.isFree(new Position(i, j))))
                    {
                        if(this.getPiece(new Position(i, j)).getColor())
                        {
                            //Fichas blancas
                            stTablero.append("[O]|");
                        }else
                        {
                            //Fichas negras
                            stTablero.append("[X]|");
                        }
                    }else if((j%2 == 1) && (this.isFree(new Position(i, j))))
                    {
                        stTablero.append("   |");
                    }else if ((j%2 == 1) && (!this.isFree(new Position(i, j))))
                    {
                        if(this.getPiece(new Position(i, j)).getColor())
                        {
                            //Fichas blancas
                            stTablero.append(" O |");
                        }else
                        {
                            //Fichas negras
                            stTablero.append(" X |");
                        }
                    }
                }else
                {
                    if((j%2 == 0) && (this.isFree(new Position(i, j))))
                    {
                        stTablero.append("   |");
                    }else if ((j%2 == 0) && (!this.isFree(new Position(i, j))))
                    {
                        if(this.getPiece(new Position(i, j)).getColor())
                        {
                            //Fichas blancas
                            stTablero.append(" O |");
                        }else
                        {
                            //Fichas negras
                            stTablero.append(" X |");
                        }
                    }else if((j%2 == 1) && (this.isFree(new Position(i, j))))
                    {
                        stTablero.append("[ ]|");
                    }else if ((j%2 == 1) && (!this.isFree(new Position(i, j))))
                    {
                        if(this.getPiece(new Position(i, j)).getColor())
                        {
                            //Fichas blancas
                            stTablero.append("[O]|");
                        }else
                        {
                            //Fichas negras
                            stTablero.append("[X]|");
                        }
                    }
                }
            }
            if(i != CASILLASTAB - 1)
                stTablero.append("\n|");
        }
        
        stReturnable = new String(stTablero);
        
        return stReturnable;
    }
    
    /**
     * Comprobaciones manuales del juego
     * @return
     * @throws IOException
     */
    public Position seleccionarFicha () throws IOException{
            
            BufferedReader in = new BufferedReader (new InputStreamReader (System.in));
            int x, y;
            String aux;
            
            System.out.println("Introduce la coordenada x: ");
            aux = in.readLine();
            x = Integer.parseInt(aux);
            
            System.out.println("Introduce la coordenada y: ");
            aux = in.readLine();
            y = Integer.parseInt(aux);
            
            return new Position(x, y);
        }
    
     /**
     * Comprobaciones del programador
     * @param peon
     * @return
     * @throws IOException
     */
    public Position avanzarAPeon(Peon peon) throws IOException{
         
        BufferedReader in = new BufferedReader (new InputStreamReader (System.in));
        Position pos = new Position(peon.getPosition().getX(), peon.getPosition().getY());
        String aux;
        int y;
        
        System.out.println("Hacia donde avanzar D(1) o I(0): ");
        aux = in.readLine();
        y = Integer.parseInt(aux);
        
        if(peon.getColor() == true){
            if(y == 1){
                pos = new Position(peon.getPosition().getX()+1, peon.getPosition().getY() - 1);
            }
            else
            {
                pos = new Position(peon.getPosition().getX()+1, peon.getPosition().getY() + 1);
            }
        }
        else if(peon.getColor() == false){
            if(y == 1){
                pos = new Position(peon.getPosition().getX()-1, peon.getPosition().getY() + 1);
            }
            else
            {
                pos = new Position(peon.getPosition().getX()-1, peon.getPosition().getY() - 1);
            }
        }
        
        return pos;
     }
}

