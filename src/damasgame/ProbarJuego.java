/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package damasgame;

import java.io.IOException;

/**
 *
 * @author VictorCG
 */
public class ProbarJuego {
    public static void main(String[] args) throws IOException {
        
        // TODO code application logic here
        Jugador jug = new Jugador("Juan", true, false, false);
        Partida partida = new Partida(jug,true);
        Position posFicha;
        Position posMov;

        while(true){
            System.out.println("Partida: \n" + partida.getReglas());
            posFicha = partida.getReglas().seleccionarFicha();
            posMov = partida.getReglas().seleccionarFicha();
            partida.getReglas().play(posFicha, posMov, partida);
            System.out.println("Partida: \n" + partida.getReglas());
        }
    }
}
