/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package damasgame;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author masterinftel22
 */
public class DamasGame extends JFrame {
    
    private static GraphInterface viewBoard;
    
    public DamasGame() {
        this.setSize(530, 450);
        this.setTitle("Damas Game");

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });
        
        //Create board
        viewBoard = new GraphInterface(this);
        Container c = getContentPane();
        this.setResizable(false);
        viewBoard.createBoardGUI(this);
        viewBoard.setJMenu(this);;
        show();
    }
    
    /**Get instance of DamasGame. This class is a singleton*/
    public static GraphInterface getInstance ()
    {
        if(viewBoard == null)
        {
            viewBoard = new GraphInterface();
        }
        
        return viewBoard;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        new DamasGame();
    }
}
