package damasgame;

import java.io.Serializable;

/**
 *
 * @author Ana Belén
 */
public class Dama extends Peon implements Serializable{
    
    /**
     * Posicion de la ficha
     */
    private Position position;
    
    private static final long serialVersionUID = 1234567L;
    
    /**
     * Color de la ficha
     */
    private boolean color;
    
    /**
     * Constructor
     * @param c
     * @param pos 
     */
    public Dama (boolean c, Position pos) {
        super (c,pos);
        this.color = c;
        this.position = pos;
    }

    /**
     * Devuelve el color de la dama
     * @return color
     */
    @Override
    public boolean getColor () {
        return color;
    }
    
    /**
     * Le asigna un color a la dama
     * @param val 
     */
    @Override
    public void setColor (boolean val) {
        this.color = val;
    }
    
    /**
     * Devuelve la posicion
     * @return pos
     */
    @Override
    public Position getPosition () {
        return position;
    }
    
    /**
     * Le asigna una posicion a la dama
     * @param val 
     */
    @Override
    public void setPosition (Position val) {
        this.position = val;
    }
}