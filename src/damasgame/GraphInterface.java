/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package damasgame;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 *
 * @author Juan
 */
public class GraphInterface extends JPanel implements MouseListener {
    
    
    private static GraphInterface instance;
    
    //Info players
    private static JLabel jug1;
    private static JLabel nameJug1;
    private static JLabel punctJug1;
    private static JLabel jug2;
    private static JLabel nameJug2;
    private static JLabel punctJug2;
    
    //Pieces and board
    private static JPanel tableroJP;
    private static JFrame mainFrame;
    private Peon[][] piezas;
    private JButton[][] casillasJP;
    
    //Game
    private static Partida partida;
    private static boolean isSetPos1,isSetPos2;
    private static boolean isRecovery = false;
    private static Position pos1, pos2;
    private String jugador;
    
    private String language, country;
    
    //Coordinates
    private static int i,j;
    
    private static final String stBundle_Name = GraphInterface.class.getPackage().getName() + ".MessageResourceBundle";
    private static Locale locale;
    private static ResourceBundle i18n;
        
    public GraphInterface()
    {
        locale = Locale.getDefault();
        i18n = ResourceBundle.getBundle(stBundle_Name,locale);
        
        if (!isRecovery) {
            
            String[] typeDamas = {"Damas Españolas","Damas Inglesas"};
            String damasEngSpa = new String();
            
            damasEngSpa = (String) JOptionPane.showInputDialog(mainFrame, 
                    i18n.getString("damasType"), i18n.getString("damasTypeMessage"), 
                    JOptionPane.QUESTION_MESSAGE, null, typeDamas, null);
            jugador = JOptionPane.showInputDialog(i18n.getString("playerName"));
            
            if(damasEngSpa.equalsIgnoreCase("Damas Inglesas"))
            {
                partida = new Partida(new Jugador(jugador, true, false, false), true);
                
            }else
            {
                partida = new Partida(new Jugador(jugador, true, false, false), false);
                
            }
        }
        
        
        isRecovery = false;
        piezas = new Peon[8][8];
        casillasJP = new JButton[8][8];
        isSetPos1 = false;
        isSetPos2 = false;
    }
    
    public GraphInterface(JFrame f)
    {
        this();
        
        Container c = f.getContentPane();
        
        //Set frame into mainFrame
        GraphInterface.mainFrame = f;
        tableroJP = new JPanel();
        c.add(tableroJP,"Center");
        
    }
    
    public void addPiece (Peon peon, Position p)
    {
        piezas [p.getX()][p.getY()] = peon;
    }
    
    public void createBoardGUI(JFrame f)
    {
        tableroJP.setLayout(new GridLayout(8, 8));
        SpringLayout layout = new SpringLayout();
        Container container = mainFrame.getContentPane();
        container.setLayout(layout);
  
        
        
        for(int x = 0; x < 8;x++)
        {
            for (int y = 0; y < 8; y++)
            {
                JButton jp = new JButton();
                
                if (partida.getIsEnglishVersion()) {

                    if ((x + y) % 2 == 0) {
                        jp.setBackground(Color.white);

                    } else {
                        if ((Partida.getReglas().getPiece(new Position(x, y)) != null) && (!Partida.getReglas().getPiece(new Position(x, y)).getColor())) {
                            
                            jp.setIcon(new ImageIcon("bullet_grey.png"));
                        } else if ((Partida.getReglas().getPiece(new Position(x, y)) != null) && (Partida.getReglas().getPiece(new Position(x, y)).getColor())) {
                            jp.setIcon(new ImageIcon("bullet_red.png"));
                        }
                        jp.setBackground(Color.black);
                    }
                } else {
                    if ((x + y) % 2 == 0) {
                        if ((Partida.getReglas().getPiece(new Position(x, y)) != null) && (!Partida.getReglas().getPiece(new Position(x, y)).getColor())) {
                            
                            jp.setIcon(new ImageIcon("bullet_grey.png"));
                        } else if ((Partida.getReglas().getPiece(new Position(x, y)) != null) && (Partida.getReglas().getPiece(new Position(x, y)).getColor())) {
                            jp.setIcon(new ImageIcon("bullet_red.png"));
                        }
                        jp.setBackground(Color.white);

                    } else {

                        jp.setBackground(Color.black);
                    }
                }
                
                
                
                jp.setActionCommand((x+","+y));
                jp.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        
                        String stPos = e.getActionCommand();
                        System.out.println("Position: " + stPos);
                        
                        String [] arrPos = stPos.split(",");
                        
                        if(!isSetPos1)
                        {
                            pos1 = new Position(Integer.parseInt(arrPos[0]), Integer.parseInt(arrPos[1]));
                            isSetPos1 = true;
                            
                            //Set coordinates
                            GraphInterface.i = Integer.parseInt(arrPos[0]);
                            GraphInterface.j = Integer.parseInt(arrPos[1]);
                        }else if (!isSetPos2)
                        {
                            pos2 = new Position(Integer.parseInt(arrPos[0]), Integer.parseInt(arrPos[1]));
                            isSetPos2 = true;
                        }     

                        //reglas.moverFicha(peon, new Position(3, 3));
                        //Partida.getReglas().get
                        //createBoardGUI();
                        System.out.println(Partida.getReglas().getPiece(new Position(Integer.parseInt(arrPos[0]),Integer.parseInt(arrPos[1]))));
                        //System.out.println(new Position(Integer.parseInt(arrPos[0]),Integer.parseInt(arrPos[1])));
                       
                    }
                });
                jp.setBorderPainted(false);
                jp.setOpaque(true);
                jp.setPreferredSize(new Dimension(45, 45));
                jp.addMouseListener(this);
                jp.validate();
                
                casillasJP[x][y] = jp;
                tableroJP.add(casillasJP[x][y]);
            }
        }
        
        //Board Features
        container.add(tableroJP);
        container.validate();
        
        
        //Adding labels
        jug1 = new JLabel(i18n.getString("label.Player1"));
        container.add(jug1);
        nameJug1 = new JLabel(i18n.getString("label.Computer"));
        container.add(nameJug1);
        //punctJug1 = new JLabel("0");
        //container.add(punctJug1);
        
        //Set features layout
        //Board
        layout.putConstraint(SpringLayout.NORTH, tableroJP, 15, SpringLayout.NORTH, container);
        layout.putConstraint(SpringLayout.WEST, tableroJP, 15, SpringLayout.WEST, container);
        
        //Label jug1
        layout.putConstraint(SpringLayout.WEST, jug1, 15, SpringLayout.EAST, tableroJP);
        layout.putConstraint(SpringLayout.NORTH, jug1, 15, SpringLayout.NORTH, container);
        
        //Label Name player
        layout.putConstraint(SpringLayout.WEST, nameJug1, 5, SpringLayout.EAST, jug1);
        layout.putConstraint(SpringLayout.NORTH, nameJug1, 15, SpringLayout.NORTH, container);
        
        //Adding labels player 2
        jug2 = new JLabel(i18n.getString("label.Player2"));
        container.add(jug2);
        nameJug2 = new JLabel(Partida.getJugador1().getName());
        container.add(nameJug2);
        
        //Set features layout
        //Label jug2
        layout.putConstraint(SpringLayout.WEST, jug2, 15, SpringLayout.EAST, tableroJP);
        
        //Label Name player
        layout.putConstraint(SpringLayout.WEST, nameJug2, 5, SpringLayout.EAST, jug2);
    }
    
    public static void updateBoard(GraphInterface graph)
    {
        JButton[][] arrCasillasJP = new JButton[8][8];
        tableroJP.removeAll();
        tableroJP.revalidate();
        tableroJP.setLayout(new GridLayout(8, 8));
        
        SpringLayout layout = new SpringLayout();
        Container container = mainFrame.getContentPane();
        
        container.setLayout(layout);
        
        for(int x = 0; x < 8;x++)
        {
            for (int y = 0; y < 8; y++)
            {
                JButton jp = new JButton();
                
                if((x+y)%2 == 0)
                {
                    jp.setBackground(Color.white);
                    
                    
                }else
                {   
                    
                    jp.setBackground(Color.black);
                }
                
                
                if ((Partida.getReglas().getPiece(new Position(x, y)) != null) && (!Partida.getReglas().getPiece(new Position(x, y)).getColor())) {
                    
                    if (Partida.getReglas().isPeon(Partida.getReglas().getPiece(new Position(x, y)))) {
                        jp.setIcon(new ImageIcon("bullet_grey_Dama.png"));
                    } else {
                        jp.setIcon(new ImageIcon("bullet_grey.png"));
                    }

                } else if ((Partida.getReglas().getPiece(new Position(x, y)) != null) && (Partida.getReglas().getPiece(new Position(x, y)).getColor())) {
                    if (Partida.getReglas().isPeon(Partida.getReglas().getPiece(new Position(x, y)))) {
                        jp.setIcon(new ImageIcon("bullet_red_Dama.png"));
                    } else {
                        jp.setIcon(new ImageIcon("bullet_red.png"));
                    }
                }
                
                
                jp.setActionCommand((x+","+y));
                jp.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        
                        String stPos = e.getActionCommand();
                        System.out.println("Position: " + stPos);
                        
                        String [] arrPos = stPos.split(",");
                        
                        if(!isSetPos1)
                        {
                            pos1 = new Position(Integer.parseInt(arrPos[0]), Integer.parseInt(arrPos[1]));
                            isSetPos1 = true;
                            
                            //Set coordinates
                            GraphInterface.i = Integer.parseInt(arrPos[0]);
                            GraphInterface.j = Integer.parseInt(arrPos[1]);
                        }else if (!isSetPos2)
                        {
                            pos2 = new Position(Integer.parseInt(arrPos[0]), Integer.parseInt(arrPos[1]));
                            isSetPos2 = true;
                        }     

                        System.out.println(Partida.getReglas().getPiece(new Position(Integer.parseInt(arrPos[0]),Integer.parseInt(arrPos[1]))));
                    }
                });
                jp.setBorderPainted(false);
                jp.setOpaque(true);
                jp.setPreferredSize(new Dimension(45, 45));
                jp.addMouseListener(graph);
                jp.validate();
                
                arrCasillasJP[x][y] = jp;
                tableroJP.add(arrCasillasJP[x][y]);
            }
        }
        
        //Board Features
        container.add(tableroJP);
        container.validate();
        tableroJP.repaint();
        
        //Delete labels
        container.remove(jug1);
        container.remove(nameJug1);
        container.remove(jug2);
        container.remove(nameJug2);
        
        //Adding labels
        jug1 = new JLabel(i18n.getString("label.Player1"));
        container.add(jug1);
        nameJug1 = new JLabel(i18n.getString("label.Computer"));
        container.add(nameJug1);
        
        //Set features layout
        //Board
        layout.putConstraint(SpringLayout.NORTH, tableroJP, 15, SpringLayout.NORTH, container);
        layout.putConstraint(SpringLayout.WEST, tableroJP, 15, SpringLayout.WEST, container);
        
        //Label jug1
        layout.putConstraint(SpringLayout.WEST, jug1, 15, SpringLayout.EAST, tableroJP);
        layout.putConstraint(SpringLayout.NORTH, jug1, 15, SpringLayout.NORTH, container);
        
        //Label Name player
        layout.putConstraint(SpringLayout.WEST, nameJug1, 5, SpringLayout.EAST, jug1);
        layout.putConstraint(SpringLayout.NORTH, nameJug1, 15, SpringLayout.NORTH, container);
        
        //Adding labels player 2
        jug2 = new JLabel(i18n.getString("label.Player2"));
        container.add(jug2);
        nameJug2 = new JLabel(Partida.getJugador1().getName());
        container.add(nameJug2);
        
        //Set features layout
        layout.putConstraint(SpringLayout.WEST, jug2, 15, SpringLayout.EAST, tableroJP);
        
        //Label Name player
        layout.putConstraint(SpringLayout.WEST, nameJug2, 5, SpringLayout.EAST, jug2);
    }
    
    public void setJMenu(JFrame f)
    {
        JMenuBar menuDamas = new JMenuBar();
        
        JMenu file = new JMenu(i18n.getString("label.File"));
        file.setMnemonic(KeyEvent.VK_F);
        
        JMenu help = new JMenu(i18n.getString("label.Help"));
        help.setMnemonic(KeyEvent.VK_H);
        
        //File menu
        //New game
        JMenuItem jmNewGame = new JMenuItem(i18n.getString("label.NewGame"));
        jmNewGame.setToolTipText(i18n.getString("label.CreateNew"));
        jmNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Create a new game.");
                updateBoard(new GraphInterface());
                Reglas regla = Partida.getInstance();
                
                System.out.println("Regla NUEVA: \n" + regla);
                nameJug2.setText(Partida.getJugador1().getName());
                
            }
        });
        
        //Save Game
        JMenuItem jmSaveGame = new JMenuItem(i18n.getString("label.SaveGame"));
        jmSaveGame.setToolTipText(i18n.getString("label.SaveText"));
        jmSaveGame.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Game saved.");
                partida.saveGame();
                JOptionPane.showMessageDialog(null,i18n.getString("label.SaveText"));
            }
        });
        
        //Recovery Game
        JMenuItem jmRecovery = new JMenuItem(i18n.getString("label.RecoveryGame"));
        jmRecovery.setToolTipText(i18n.getString("label.RecoveryText"));
        jmRecovery.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                partida = new Partida(true);
                Partida.recoveryGame();
                System.out.println("Partida: " + partida.getReglas());
                isRecovery = true;
                updateBoard(GraphInterface.getInstance());
                System.out.println("Partida: " + partida.getReglas());
                Reglas regla = Partida.getInstance();
                
                JOptionPane.showMessageDialog(null, i18n.getString("label.RecoveryText"));
                nameJug2.setText(Partida.getJugador1().getName());
            }
        });
        
        //Exit
        JMenuItem jmExit = new JMenuItem(i18n.getString("label.Exit"));
        jmExit.setToolTipText(i18n.getString("label.ExitText"));
        jmExit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        //Help menu
        //About
        JMenuItem jmAbout = new JMenuItem(i18n.getString("label.About"));
        jmAbout.setToolTipText(i18n.getString("label.AboutText"));
        jmAbout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Show emergent window");
                JOptionPane.showMessageDialog(null, i18n.getString("label.Thisaplication") + "\nAna Belén Ramirez Borrego\nVíctor Caballero García\nJuan Cañete Rodríguez");
            }
        });
        
        //Add buttons
        file.add(jmNewGame);
        file.addSeparator();
        file.add(jmSaveGame);
        file.add(jmRecovery);
        file.addSeparator();
        file.add(jmExit);
        
        help.add(jmAbout);
        
        menuDamas.add(file);
        menuDamas.add(help);
        menuDamas.setSize(500, 320);
        
        
        f.setJMenuBar(menuDamas);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
        f.setVisible(true);
        
    }
    
    public void setPartida(Partida partida) {
        this.partida = partida;
    }

    public JPanel getTableroJP() {
        return tableroJP;
    }
    
    /**
     * Get instance of Reglas. This class is a singleto
     *
     * @return n
     */
    public static GraphInterface getInstance() {
        if (instance == null) {
            instance = new GraphInterface();
        }

        return instance;
    }
    
    //MouseListener
    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("Button clicked");

        int val;

        try {
            if (partida.getWinner() == 0) {

                if (isSetPos1 && isSetPos2) {
                    Peon peon = Partida.getReglas().getPiece(pos1);
                    Reglas reglas = Partida.getInstance();

                    reglas.play(pos1, pos2, partida);

                    //We Move piece
                    JButton b;

                    for (int x = 0; x < 8; x++) {
                        for (int y = 0; y < 8; y++) {
                            val = (x * 8) + y;
                            b = (JButton) tableroJP.getComponent(val);

                            if (reglas.getPiece(new Position(x, y)) != null) {
                                if (reglas.getPiece(new Position(x, y)).getColor()) {

                                    if (reglas.isPeon(Partida.getReglas().getPiece(new Position(x, y)))) {
                                        b.setIcon(new ImageIcon("bullet_red_Dama.png"));
                                    } else {
                                        b.setIcon(new ImageIcon("bullet_red.png"));
                                    }

                                } else {
                                    if (reglas.isPeon(Partida.getReglas().getPiece(new Position(x, y)))) {
                                        b.setIcon(new ImageIcon("bullet_grey_Dama.png"));
                                    } else {
                                        b.setIcon(new ImageIcon("bullet_grey.png"));
                                    }
                                }
                            } else {
                                b.setIcon(null);
                            }

                        }
                    }

                    System.out.println("Reglas durante juego: \n" + reglas);

                    //Init variables
                    isSetPos1 = false;
                    isSetPos2 = false;
                }

                tableroJP.repaint();
            } else {
                if (partida.getWinner() == 1) {
                    JOptionPane.showMessageDialog(null, i18n.getString("winner1"));
                } else if (partida.getWinner() == 2) {
                    JOptionPane.showMessageDialog(null, i18n.getString("winner2"));
                } else if (partida.getWinner() == 3) {
                    JOptionPane.showMessageDialog(null, i18n.getString("draw"));
                }

            }
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Movimiento no permitido");
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
