package damasgame;

import java.io.Serializable;

/**
 *
 * @author Ana Belén
 */
public class Position implements Serializable{

    private static final long serialVersionUID = 1234567L;
    /**
     * Posicion x
     */
    private int x;
    
    /**
     * Posicion y
     */
    private int y;
    
    /**
     * Constructor
     * @param x0
     * @param y0 
     */
    public Position (int x0, int y0) {
       this.x = x0;
       this.y = y0;
    }
    
    /**
     * Devuelve el valor de x
     * @return x
     */
    public int getX () {
        return x;
    }
    
    /**
     * Asigna un valor a x
     * @param val 
     */
    public void setX (int val) {
        this.x = val;
    }
    
    /**
     * Devuelve el valor de y
     * @return y
     */
    public int getY () {
        return y;
    }

    /**
     * Asigna un valor a y
     * @param val 
     */
    public void setY (int val) {
        this.y = val;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.x;
        hash = 29 * hash + this.y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString() {
        return "Position{" + "x=" + x + ", y=" + y + '}';
    }
}

