package damasgame;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Ana Belén
 */
public class Peon implements Serializable{

    private static final long serialVersionUID = 1234567L;
    
    /**
     * Posicion
     */
    private Position position;

    
    /**
     * Color de la ficha
     */
    private boolean color;
    
    /**
     * Constructor
     * @param c
     * @param pos
     */
    public Peon (boolean c, Position pos) {
        this.color = c;
        this.position = pos;
    }
    
    /**
     * Constructor 2
     * @param c 
     * @param x 
     * @param y 
     */
    public Peon (boolean c, int x, int y) {
        this.color = c;
        this.position = new Position(x, y);
    }
    
    /**
     * Devuelve el color de la ficha
     * @return color
     */
    public boolean getColor () {
        return color;
    }
    
    /**
     * Le asigna un color a la ficha
     * @param val 
     */
    public void setColor (boolean val) {
        this.color = val;
    }
    
    /**
     * Devuelve la posicion
     * @return position
     */
    public Position getPosition () {
        return position;
    }
    
    /**
     * Le asigna un valor a la posicion
     * @param val 
     */
    public void setPosition (Position val) {
        this.position = val;
    }
    
    /**
     * Mueve la ficha a la posicion indicada
     * @param val 
     */
    public void mover (Position val) {
        this.position = val;
    }

    /**
     * Color y posicion de peon
     * @return 
     */
    @Override
    public String toString() {
        return "Peon{" + "color=" + color + ", posicion=" + position + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.position);
        hash = 23 * hash + (this.color ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Peon other = (Peon) obj;
        if (!Objects.equals(this.position, other.position)) {
            return false;
        }
        if (this.color != other.color) {
            return false;
        }
        return true;
    }
    
    
}

