/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package damasgame;

import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Ana Belén
 */
public class PartidaTestRunner {
    
    private static Reglas reglas;
    private static Jugador jugador1,jugador2;
    //Punctuations
    private static Integer puntJug1 = 0, puntJug2 = 0;
    
     @BeforeClass
        public static void setUpClass() throws Exception{   
        }

        @AfterClass
        public static void tearDownClass() throws Exception {
        }
        
        @Test
        public void GetReglasCheck() {
            System.out.println("* PartidaTest: GetReglasCheck()");
            Reglas expResult = reglas;
            Reglas result = Partida.getReglas();
            assertEquals(expResult, result);
            assertEquals(expResult, result);
        }
        
     /**
     * Test of getJugador1 method, of class Partida.
     */
    @Test
    public void GetJugador1Check() {
        System.out.println("* PartidaTest: GetJugador1Check()");
        Jugador expResult = jugador1;
        Jugador result = Partida.getJugador1();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJugador2 method, of class Partida.
     */
    @Test
    public void GetJugador2Check() {
        System.out.println("* PartidaTest: GetJugador2Check()");
        Jugador expResult = jugador2;
        Jugador result = Partida.getJugador2();
        assertEquals(expResult, result);
    }
    
     /**
     * Test of getPuntJug1 method, of class Partida.
     */
    @Test
    public void GetPuntJug1Check() {
        System.out.println("* PartidaTest: GetPuntJug1Check()");
        int expResult = puntJug1;
        int result = Partida.getPuntJug1();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getPuntJug2 method, of class Partida.
     */
    @Test
    public void GetPuntJug2Check() {
        System.out.println("* PartidaTest: GetPuntJug2Check()");
        int expResult = puntJug2;
        int result = Partida.getPuntJug2();
        assertEquals(expResult, result);
    }
    
 
      
}
